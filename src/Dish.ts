export class Dish {

  name: string;

  vegetarian: boolean;

  ingredients: string[];
}
