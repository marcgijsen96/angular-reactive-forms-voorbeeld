import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from "@angular/forms";

@Component({
  selector: 'app-dish-form',
  templateUrl: './dish-form.component.html',
  styleUrls: ['./dish-form.component.css']
})
export class DishFormComponent implements OnInit {
  dishForm = new FormGroup({
    // Name: ["", Validators.required, Validators.pattern(new RegExp("^[a-zA-Z]{2,255}$"))],
    // Vegetarian: [false],
    // MainIngredient: ["", Validators.required, Validators.pattern("^[a-zA-Z]{2,255}$")]
    name: new FormControl("", Validators.required),
    isVegetarian: new FormControl(null),
    mainIngredient: new FormControl("", [
      Validators.required,
      Validators.pattern('^[a-zA-Z]{3,255}$')
    ])
  });

  constructor() {

  }

  onSubmit() {
    console.info(this.dishForm.value);
    alert(JSON.stringify(this.dishForm.value))
  }

  ngOnInit() {
    this.dishForm.valueChanges.subscribe(console.log);
  }

}
