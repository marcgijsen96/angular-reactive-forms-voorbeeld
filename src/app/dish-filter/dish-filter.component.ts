import {Dish} from "../../Dish";
import {Component, OnInit} from "@angular/core";
import {FormControl} from "@angular/forms";
import {dishes} from "../../MockData";


@Component({
  selector: 'app-dish-filter',
  templateUrl: './dish-filter.component.html',
  styleUrls: ['./dish-filter.component.css']
})

export class DishFilterComponent implements OnInit {

  dishes: Dish[] = dishes;

  name = new FormControl("");
  vega = new FormControl("");

  constructor() {
  }

  ngOnInit(): void {

  }

  getFilteredDishes(): Dish[] {
    if (!this.vega.value && (this.name.value === undefined || this.name.value === null || this.name.value === '' || this.name.value === ""))
      return dishes;

    else
      return dishes.filter((value) => value.name.toLowerCase().startsWith(this.name.value.toLowerCase()) && ((this.vega.value && value.vegetarian) || !this.vega.value));
  }
}

