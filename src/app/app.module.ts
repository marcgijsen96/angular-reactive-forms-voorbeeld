import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DishFilterComponent } from './dish-filter/dish-filter.component';
import { DishFormComponent } from './dish-form/dish-form.component';

@NgModule({
  declarations: [
    AppComponent,
    DishFilterComponent,
    DishFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
