import {Dish} from "./Dish";

export const dishes : Dish[] = [
  {name: 'Spaghetti', vegetarian: false, ingredients: ['meat', 'pasta', 'tomatoes']},
  {name: 'Fish & Chips', vegetarian: true, ingredients: ['Potatoes', 'Fish', 'Salt']},
  {name: 'Rice', vegetarian: true, ingredients: ['Rice']},
  {name: 'Hamburger', vegetarian: false, ingredients: ['meat', 'bread', 'cheese']},
  {name: 'Stoofpotje', vegetarian: false, ingredients: ['beef', 'carrots', 'onion']},
  {name: 'Goulash', vegetarian: false, ingredients: ['beef', 'beer']},
  {name: 'Boerenkool', vegetarian: true, ingredients: ['potatoes', 'kale']}
];


